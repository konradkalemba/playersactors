import akka.actor.{ ActorRef, ActorSystem, Props }

object Main {   
  val PlayersNumber = 3

  def main(args: Array[String]){
    val system = ActorSystem()
    val players = new Array[(Int, ActorRef)](PlayersNumber)
    
    for (i <- 0 until PlayersNumber) {
      players(i) = (i, system.actorOf(Props(classOf[Player], i)))
    }

    players.foreach { _._2 ! Player.Start(players) }
  }
}
