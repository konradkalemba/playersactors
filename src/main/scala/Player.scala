import akka.actor.{ Actor, ActorRef }
import scala.util._

object Player {
  case class Start(players: Array[(Int, ActorRef)])
  case object Stop
  private case class Ball(var count: Int = 0)
}

class Player(val id: Int) extends Actor { 
  import Player._
  private var opponents = Array[(Int, ActorRef)]()

  def receive() = {
    case Start(players) => {
      opponents = players.filter(_._2 != self)

      // First player starts the game
      if (players(0)._2 == self) {
        pass(new Ball())
      }
    }
    case ball: Ball => pass(ball)
    case Stop => stop
    case _ => println("Wrong request") 
  }

  private def pass(ball: Ball) {
    val opponent = opponents(Random.nextInt(opponents.size))

    ball.count += 1
    
    println("Player #%d passes (#%d) ball to player #%d".format(id, ball.count, opponent._1))
    opponent._2 ! ball
  }

  private def stop() = {
    context.stop(self)
    println("Player #%d stopped".format(id))
  }
}
